﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {
	public GameObject TilePrefabBlack;
	public GameObject TilePrefabWhite;
//	GameManager gameManager;

	// Use this for initialization
	void Start () {
//		gameManager = GameObject.Find ("GameManager").GetComponent<GameManager> ();
		createBoard ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void createBoard() {
		List<Tile> board = new List<Tile> ();

		int gridSize = 64;

		Vector3 startPos = new Vector3 (-3.5f, -3.5f, 0);
		Vector3 tilePos = startPos;

		Tile tile;
		int tileNum = 1;
		string tileLetter = "a";
		GameObject tilePrefab = TilePrefabWhite;

		for (int i = 0; i < gridSize; i++) {

			tilePrefab.name = tileLetter + System.Convert.ToString (tileNum);
			tile = (Instantiate (tilePrefab, tilePos, Quaternion.identity) as GameObject).GetComponent<Tile> ();

			if (i % 2 == 0) {
				
				tilePrefab = TilePrefabBlack;
//				board.Add (tile.gameObject);
			} else {
				tilePrefab = TilePrefabWhite;
			}


			if ((i+1) % 8 == 0) {
				tilePos = startPos + Vector3.right;
				startPos = startPos + Vector3.right;
				tileLetter = incrementString (tileLetter);
				tileNum = 0;
				if (tilePrefab == TilePrefabWhite) {
					tilePrefab = TilePrefabBlack;
				} else {
					tilePrefab = TilePrefabWhite;
				}

			} else {
				tilePos = tilePos + Vector3.up;
			}

			tileNum++;

		}


	}

	public string incrementString(string theString) {
		char charString = theString [0];
		charString++;
		string newString = System.Convert.ToString (charString);
		return newString;
	}

}
